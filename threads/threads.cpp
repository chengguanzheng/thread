// threads.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

std::mutex mtx;
std::condition_variable cv;
bool flag = false;

void printfunc1() {
    int i = 1;
    for (; i < 100; i+=2) {
        std::unique_lock<std::mutex> lck(mtx);
        cv.wait(lck, [](){return flag;}); //flag == ture时不会wait
        std::cout << i << " " << std::this_thread::get_id() << std::endl;
        flag = !flag; //此线程下一次cv.wait 则会 wait
        cv.notify_one();
    }

}

void printfunc2() {
    int i = 0;
    for (; i < 100; i+=2) {
        std::unique_lock<std::mutex> lck(mtx);
        cv.wait(lck, []() {return !flag; }); //flag == false时不会wait
        std::cout << i << " " << std::this_thread::get_id() << std::endl;
        flag = !flag;  //此线程下一次cv.wait 则会 wait
        cv.notify_one();
    }
}

int main()
{
    std::thread mythr1(printfunc1);
    std::thread mythr2(printfunc2);

    mythr1.join();
    mythr2.join();
    return 0;    
}
